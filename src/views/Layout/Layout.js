// https://github.com/sefyudem/Sliding-Sign-In-Sign-Up-Form
export default {
  mounted() {
    var route_index = this.get_route_index();
    this.chage_panel_css(route_index);
  },
  props: {
    source: String,
  },
  computed: {
    isMobile() {
      if (this.$vuetify.breakpoint.xsOnly) {
        return true;
      } else {
        return false;
      }
    },
    get_empty_panel_css() {
      var static_css = "nav_padding empty_panel";
      return static_css;
    },
    get_panel_css() {
      var static_css = "nav_padding panel_link";
      return static_css;
    },
  },
  watch: {
    $route() {
      this.show = false;
      var route_index = this.get_route_index();
      this.chage_panel_css(route_index);
    },
  },
  data: () => ({
    drawer: true,
    panel_route_counter: [
      {
        route: "/app/dashboard",
        css: "panel_link",
      },
      {
        route: "/app/test",
        css: "panel_link",
      },
      {
        route: "/app/test2",
        css: "panel_link",
      },
      {
        route: "/app/test3",
        css: "panel_link",
      },
    ],
    color: "#213faa",
    right: false,
    permanent: false,
    miniVariant: false,
    expandOnHover: false,
    background: false,
  }),
  methods: {
    get_route_index() {
      var route_index = 0;
      for (var i = 0; i < this.panel_route_counter.length; i++) {
        if (
          this.$router.currentRoute.path == this.panel_route_counter[i].route
        ) {
          route_index = i;
        }
      }
      return route_index;
    },
    chage_panel_css(current_route_index) {
      console.log(current_route_index);
      var temp_id = "";
      console.log(temp_id);
      //////////////////// remove rounded corner css from all panels ////////////////
      for (var i = 0; i < this.panel_route_counter.length; i++) {
        this.panel_route_counter[i].css = "panel_link";
      }
      document.getElementById("top_panel").classList.remove("above_panel");
      document.getElementById("bottom_panel").classList.remove("below_panel");

      this.set_above_and_below_panel(current_route_index);
    },
    set_above_and_below_panel(index) {
      var above = index - 1;
      var below = index + 1;
      var above_panel = "panel_" + above.toString();
      var below_panel = "panel_" + below.toString();
      if (above < 0) {
        above_panel = "top_panel";
        var above_el = document.getElementById(above_panel);
        above_el.classList.add("above_panel");
      } else {
        this.panel_route_counter[above].css = ["panel_link", "above_panel"];
      }
      if (below > this.panel_route_counter.length - 1) {
        below_panel = "bottom_panel";
        var below_el = document.getElementById(below_panel);
        below_el.classList.add("below_panel");
      } else {
        this.panel_route_counter[below].css = ["panel_link", "below_panel"];
      }
    },
  },
};
