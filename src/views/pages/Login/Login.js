export default {
  data: () => ({
    user_name: "",
    password: "",
    email: "",
  }),
  props: {
    source: String,
  },
  methods: {
    sign_in_clicked() {
      alert("sign in clicked");
      const container = document.querySelector(".container");
      container.classList.remove("sign-up-mode");
    },
    sign_up_clicked() {
      alert("sign up clicked");
      const container = document.querySelector(".container");
      container.classList.add("sign-up-mode");
    },
    login() {
      alert(this.email);
      alert(this.password);
      alert("login");
    },
    sign_up() {
      alert(this.email);
      alert(this.password);
      alert("sign_up");
    },
  },
};
