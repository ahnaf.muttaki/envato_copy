import Vue from "vue";
import VueRouter from "vue-router";
import Login from "../views/pages/Login/Login.vue";
import Layout from "../views/Layout/Layout.vue";
import Dashboard from "../views/pages/Dashboard.vue";
import Test from "../views/pages/Test.vue";
import Test2 from "../views/pages/Test2.vue";
import Test3 from "../views/pages/Test3.vue";
Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "login",
    component: Login,
  },
  {
    path: "/app",
    component: Layout,
    children: [
      {
        path: "dashboard",
        component: Dashboard,
        meta: {
          requiresAuth: false,
          requiresAdmin: false,
        },
      },
    ],
  },
  {
    path: "/app",
    component: Layout,
    children: [
      {
        path: "test",
        component: Test,
        meta: {
          requiresAuth: false,
          requiresAdmin: false,
        },
      },
    ],
  },
  {
    path: "/app",
    component: Layout,
    children: [
      {
        path: "test2",
        component: Test2,
        meta: {
          requiresAuth: false,
          requiresAdmin: false,
        },
      },
    ],
  },
  {
    path: "/app",
    component: Layout,
    children: [
      {
        path: "test3",
        component: Test3,
        meta: {
          requiresAuth: false,
          requiresAdmin: false,
        },
      },
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
  linkActiveClass: "panel_link_active",
  linkExactActiveClass: "exact-active",
});

export default router;
